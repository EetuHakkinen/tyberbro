import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  public userData;

  constructor(private http: HttpClient) { }

  getData() {
    return null;
  }

  updateData() {
    this.getData()
      .subscribe((data) => {
        this.userData = data;
      });
  }

  signIn(body){  
    console.log('send data to server,',body);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      })
    };
    return this.http.post('192.168.10.55:3001/api/signIn', body, httpOptions)
  }
}
