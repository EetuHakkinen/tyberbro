import { Component, OnInit, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { WordGeneratorService } from '../word-generator.service';
import { FormControl } from '@angular/forms';
import { DatabaseService } from '../database.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  protected wordArray;
  private currentWord;
  private index;
  protected correctWords;
  inputField = new FormControl('');
  private timerOn;
  public timeLeft;
  private interval;
  protected chars;
  public showResults;
  protected totalWords;
  time = 60;
  lang = 'fi';
  protected missedChars;
  protected totalChars;
  private startTime;
  protected speedArray;
  chart = [];
  protected allWords;

  constructor(private words: WordGeneratorService, private db: DatabaseService, private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.typed();
    this.newTest();
    this.checkWordarrayLength();
  }

  generateWords() {
    this.wordArray = this.words.generateWords(9, this.lang);
    this.currentWord = this.wordArray[0];
    this.currentWord.color = 'blue';
    this.index = 0;
    this.checkWordarrayLength();
  }

  checkWordarrayLength() {
    var array = document.getElementById('array') as HTMLElement;
    var box = document.getElementById('wordBox') as HTMLElement;
    if (array && box) {
      console.log('checking word array length');
      var sum = 0;
      while(array.clientWidth > box.clientWidth){
        var newArray = this.wordArray.slice(0, this.wordArray.length - 2);
        this.wordArray = newArray;
        array = document.getElementById('array') as HTMLElement;
        console.log('remove', this.wordArray.length, 'array', array.clientWidth, 'box', box.clientWidth);
        this.cdr.detectChanges();
      }
    }
  }

  handleLangChange(event) {
    this.lang = event.target.value;
  }

  handleTimeChange(event) {
    this.time = Number(event.target.value);
  }

  newTest() {
    clearInterval(this.interval);
    this.generateWords();
    this.timeLeft = this.time;
    this.chars = 0;
    this.correctWords = 0;
    this.inputField.setValue('');
    this.timerOn = false;
    this.showResults = false;
    this.totalWords = 0;
    this.missedChars = 0;
    this.totalChars = 0;
    this.speedArray = [];
    this.allWords = [];
    this.checkWordarrayLength();
  }

  onClick() {
    this.newTest();
    this.checkWordarrayLength();
  }

  startTimer() {
    this.checkWordarrayLength();
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft --;
        this.showResults = false;
      } else {
        this.showResults = true;
      }
    }, 1000);
  }

  typed() {
    this.inputField.valueChanges.subscribe(val => {
      this.totalChars ++;
      if (val !== this.currentWord.word.substring(0, val.length)){
        this.currentWord.color = 'red';
        this.missedChars ++;
      }
      if (this.currentWord.color === 'red' && val === this.currentWord.word.substring(0, val.length)){
        this.currentWord.color = 'blue';
      }
      if (!this.timerOn && val !== '') {
        this.startTimer();
        this.timerOn = true;
        this.startTime = new Date().getTime();
      }
      if (val.charAt(val.length - 1) === ' ') {
        const time = new Date().getTime() - this.startTime;
        this.handleTime(time, val.length);
        this.checkWord(val.replace(/\s/g, ''));
        this.startTime = new Date().getTime();
      }
    });
  }

  checkWord(word) {
    this.totalWords ++;
    this.allWords.push(word);
    this.inputField.setValue('');
    if (word === this.currentWord.word && this.timeLeft > 0) {
      this.wordArray[this.index].color = 'green';
      this.correctWords ++;
      this.chars += word.length;
    } else if (this.timeLeft > 0) {
      this.wordArray[this.index].color = 'red';
    }
    this.index ++;
    if (this.wordArray.length > this.index) {
      this.currentWord = this.wordArray[this.index];
      this.currentWord.color = 'blue';
    } else if (this.timeLeft > 0) {
      this.generateWords();
    }

  }

  getMistakes() {
    const misWords = this.totalWords - this.correctWords;
    const percent = misWords * 100 / this.totalWords;
    const rounded = Math.round(percent * 10) / 10;
    return rounded;
  }

  getMissedChars() {
    const percent = this.missedChars * 100 / this.totalChars;
    const rounded = Math.round(percent * 10) / 10;
    return rounded;
  }

  handleTime(time, chars) {
    const seconds = time / 1000;
    const charsInSecond = chars / seconds;
    const wordsPerMinute = charsInSecond * 60 / 5;
    this.speedArray.push(wordsPerMinute);
    console.log(wordsPerMinute);
  }

}
