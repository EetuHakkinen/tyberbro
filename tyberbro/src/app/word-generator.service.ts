import { Injectable } from '@angular/core';
import { finnishWords } from './finnishWords';
import { englishWords } from './englishWords';

@Injectable({
  providedIn: 'root'
})
export class WordGeneratorService {

  constructor() { }

  generateWords(q, l) {
    let words = [];
    const randomWords = [];
    if (l === 'fi') {
      words = finnishWords;
    } else if (l === 'en'){
      words = englishWords;
    }
    for (let i = 0; i < q; i ++) {
      const nmbr = Math.floor(Math.random() * Math.floor(words.length));
      randomWords.push({
        'word': words[nmbr],
        'color': 'black'
      });
    }
    return randomWords;
  }
}
