import { Component, OnInit } from '@angular/core';
import { DatabaseService } from '../database.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  public id_token;

  constructor(private db: DatabaseService) { }

  ngOnInit() {
  }

  onSignIn(googleUser){
    // The ID token you need to pass to your backend:
    this.id_token = googleUser.getAuthResponse().id_token;
    console.log("ID Token: " + this.id_token);
    var obj = {
      idToken: this.id_token
    }
    this.db.signIn(obj);
  }

}
